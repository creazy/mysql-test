const msql = require('mysql2');

const mysql = msql.createConnection({
	host: 'localhost',
	user: 'mysql',
	password: '1234',
	database: 'mysql_test',
	socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock'
});

// function query (input) {
// 	const _defaults = {
// 		params: []
// 	};
// 	const { sql, params } = Object.assign(_defaults, input);
//
// 	return new Promise((resolve, reject) => {
// 		mysql.query(sql, params, (err, resp) => {
// 			if (err) {
// 				return reject(err);
// 			}
// 			resolve(resp);
// 		});
// 	});
// }

// function test () {
// 	const sql = `
//         START TRANSACTION;
//         UPDATE user SET balance = balance - 50 WHERE id = 1 AND balance >= 50 LIMIT 1;
//         COMMIT;`;
// 	query({ sql }).then(ret => {
// 		console.log('Success', ret);
// 	}).catch(e => {
// 		console.error('Failed', e);
// 	});
// }

function test3() {
	mysql.beginTransaction(() => {
		mysql.query('UPDATE user SET balance = balance - 50 WHERE id = 1 AND balance >= 50 LIMIT 1', (err, ret) => {
			if (err) {
				mysql.rollback(() => {
					throw err;
				});
			}
			mysql.commit(err => {
				if (err) {
					mysql.rollback(() => {
						throw err;
					});
				}
				console.log('Transaction Complete.', ret);
			});
		});
	});
}

function test2() {
	mysql.query('UPDATE user SET balance = balance + 5 WHERE id = 2 LIMIT 1');
}

// test(); // success -> changed rows 1
// test(); // success -> changed rows 0
test3(); // success -> affectedRows 1
test3(); // success -> affectedRows 0
test2(); // success -> changed rows 1
